class cernbox::install {
  if $facts['os']['name'] == 'AlmaLinux' {
    $flavour = 'al'
  } else {
    $flavour = 'el'
  }
  if ($::operatingsystemmajrelease == '7') {
    $gpgkey = "http://cernbox.cern.ch/cernbox/doc/Linux/repo/CentOS_${::operatingsystemmajrelease}/repodata/repomd.xml.key"
    $baseurl = "http://cernbox.cern.ch/cernbox/doc/Linux/repo/CentOS_${::operatingsystemmajrelease}"
  } else {
    $gpgkey = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2"
    $baseurl = "https://linuxsoft.cern.ch/internal/repos/cernbox${::operatingsystemmajrelease}${flavour}-stable/${facts['os']['architecture']}/os/"
  }
  yumrepo{'cernbox':
    descr         => 'CERNBOX2.0 client repository (EOS backend)',
    baseurl       => $baseurl,
    gpgcheck      => 1,
    gpgkey        => $gpgkey,
    repo_gpgcheck => 0,
    enabled       => 1,
    before        => Package['cernbox-client'],
  }
  package { 'cernbox-client':
    ensure => present,
  }
  # Remove remnants of previous appimage deployment
  file { [ '/usr/local/bin/cernbox', '/usr/local/appimages/cernbox.AppImage', '/usr/local/share/icons/cernbox-icon.png' ]:
    ensure => absent,
  }
}
