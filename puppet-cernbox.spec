Name:           puppet-cernbox
Version:        2.8
Release:        1%{?dist}
Summary:        Masterless puppet module for cernbox client

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-agent

%description
Masterless puppet module for cernbox

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/cernbox/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/cernbox/
touch %{buildroot}/%{_datadir}/puppet/modules/cernbox/linuxsupport

%files -n puppet-cernbox
%{_datadir}/puppet/modules/cernbox
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> - 2.8-1
- Add autoreconfigure %post script

* Mon Jun 10 2024 Ben Morrice <ben.morrice@cern.ch> - 2.7-1
- Use cernbox-client RPMs instead of custom appImage deployment

* Wed Oct 11 2023 Ben Morrice <ben.morrice@cern.ch> - 2.6-1
- update appimage to v4.1.0.11373

* Tue Mar 14 2023 Ben Morrice <ben.morrice@cern.ch> - 2.5-1
- add libglvnd-glx as an appimage dependency on 9+

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> - 2.4-2
- Bump release for disttag change

* Thu Jul 21 2022 Ben Morrice <ben.morrice@cern.ch> - 2.4-1
- Add icon and desktop shortcut for appImage install

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> - 2.3-2
- add appimage depedencies

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- deploy cernbox via AppImage on CS9+

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-2
- fix requires on puppet-agent

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- release for el8

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- Add additional el8 support

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- Rebuild for el8
 
* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Tue Jul 26 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial Release
- Install cernbox client
